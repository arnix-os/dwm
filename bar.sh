black="#171a1f"
blue="#8cc1ff"
darkblue="#70a5eb"
green="#94f7c5"
gray="#b6beca"
white="#f5f5f5"

cpu() {
  cpu_val=$(grep -o "^[^ ]*" /proc/loadavg)

  printf "^c$black^ ^b$green^ CPU"
  printf "^c$white^ ^b$grey^ $cpu_val"
}

mem() {
  printf "^c$blue^^b$black^  "
  printf "^c$blue^ $(free -h | awk '/^Mem/ { print $3 }' | sed s/i//g)"
}

clock() {
	printf "^c$blue^^b$black^ $(date '+%H:%M') "
}

while true; do

  sleep 1 && xsetroot -name "$(clock)"
done
